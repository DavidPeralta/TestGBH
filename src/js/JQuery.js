

//funtion for sideNav
$(document).ready(function(){
    $(".button-start").sideNav({ 
    menuWidth: 300,
    edge: 'rigth',
    draggable: true,
    });
});
//navbar efect funtion
$(document).ready(function() {
    $(window).scroll(function() {
    if($(document).scrollTop() > 10) {
        $('#nav').addClass('shrink');
    }
    else {
    $('#nav').removeClass('shrink');
    }
    });
    $('.select-btn').material_select();
});
//scrollspy funtion 
$('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function() {
if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
  var target = $(this.hash);
  target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
  if (target.length) {
    $('html, body').animate({
      scrollTop: (target.offset().top - 75)
    }, 1500, "easeInOutExpo");
    return false;
  }
}
});
